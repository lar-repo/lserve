<?php

return [
    'auth' => [
        'hash_field' => 'ws_hash'
    ],
    'tcp_executors' => [
        'make_timers' => \Lar\LServe\TcpExecutors\MakeTimers::class
    ],
    'ssl_enable' => false,
    'ssl_config' => [
        'local_cert' => env('LOCAL_CERT', null), //'*.crt',
        'local_pk' => env('LOCAL_PK', null), //'*.key',
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    ]
];

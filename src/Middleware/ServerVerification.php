<?php

namespace Lar\LServe\Middleware;

use Closure;
use Lar\LServe\Passport;
use Lar\LServe\Server\TcpSender;

class ServerVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if (TcpSender::ping() && !Passport::isRegistered()) {

            (new TcpSender())->setRecipient(\Lar\LServe\Server\TcpCommands\Passport::class)->send(["REGISTRATION" => Passport::getMyId()]);
        }

        if (!TcpSender::ping() || $request->ajax()) {

            return $next($request);
        }

        /** @var \Illuminate\Http\Response $response */
        $response = $next($request);

        $content = $response->getContent();

        $content_with_js = str_replace("</body>", "<script id='__ProServerVerifier'>" . TcpSender::getJsConnection() . "</script>\n</body>", $content);

        $response->setContent($content_with_js);

        return $response;
    }
}

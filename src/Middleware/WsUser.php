<?php

namespace Lar\LServe\Middleware;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Lar\LServe\Passport;
use Lar\LServe\Server\TcpSender;

class WsUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $guard
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next, string $guard = "web")
    {
        $guard = Passport::addGuardIdentification($guard);

        /** @var Model $user */
        $user = $guard->user();

        $field = config('lserve.auth.hash_field', false);

        if ($user && $field) {

            if (array_search($field, $user->getFillable()) !== false) {

                $user->update([$field => Passport::getMyId()]);
            }
        }

        return $next($request);
    }
}

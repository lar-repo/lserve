<?php

namespace Lar\LServe\Middleware;

use Closure;
use Lar\Layout\Core\LConfigs;
use Lar\Layout\Respond;
use Lar\LServe\Components\ConnectionRefused;
use Lar\LServe\Passport;
use Lar\LServe\Server\TcpSender;
use Lar\LServe\Server\WebSocketServer;

class Registration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if (!TcpSender::ping()) {

            Respond::glob()->toast_error(["CONNECTION REFUSED", "LSERVE", ["timeOut" => 999999, "preventDuplicates" => true]]);
        }

        else {

            (new TcpSender())->setRecipient(\Lar\LServe\Server\TcpCommands\Passport::class)->send([
                "REGISTRATION" => Passport::getMyId(),
                "IDS" => Passport::getIds(),
                "GUARDS" => implode(",", Passport::getGuardList())
            ]);

            LConfigs::add('server', WebSocketServer::getPublicLink([

                WebSocketServer::identifyValue() => Passport::getMyId()
            ]));
        }

        /** @var \Illuminate\Http\Response $response */
        $response = $next($request);

        return $response;
    }
}

<?php

namespace Lar\LServe;

use Illuminate\Console\Command;
use Workerman\Connection\ConnectionInterface;
use Workerman\Lib\Timer;
use Workerman\Worker;

class LarWorker extends Worker
{
    /**
     * @var string
     */
    static $tcp_name;

    /**
     * @var Command
     */
    static $command;

    /**
     * Init.
     *
     * @return void
     */
    protected static function init()
    {
        set_error_handler(function($code, $msg, $file, $line){
            Worker::safeEcho("$msg in file $file on line $line\n");
        });

        // Start file.
        $backtrace        = debug_backtrace();
        static::$_startFile = $backtrace[count($backtrace) - 1]['file'];


        $unique_prefix = str_replace('/', '_', static::$tcp_name);

        // Pid file.
        if (empty(static::$pidFile)) {
            static::$pidFile = app()->bootstrapPath("$unique_prefix.pid");
        }

        // Log file.
        if (empty(static::$logFile)) {
            static::$logFile = app()->bootstrapPath("workerman.log");
        }
        $log_file = (string)static::$logFile;
        if (!is_file($log_file)) {
            touch($log_file);
            chmod($log_file, 0622);
        }

        // State.
        static::$_status = static::STATUS_STARTING;

        // For statistics.
        static::$_globalStatistics['start_timestamp'] = time();
        static::$_statisticsFile                      = sys_get_temp_dir() . "/$unique_prefix.status";

        // Process title.
        static::setProcessTitle('WorkerMan: master process  start_file=' . static::$_startFile);

        // Init data for worker id.
        static::initId();

        // Timer init.
        Timer::init();
    }

    /**
     * Exit current process.
     *
     * @return void
     */
    protected static function exitAndClearAll()
    {
        foreach (static::$_workers as $worker) {

            $socket_name = $worker->getSocketName();

            if ($worker->transport === 'unix' && $socket_name) {

                list(, $address) = explode(':', $socket_name, 2);

                @unlink($address);
            }
        }
        if (is_file(static::$pidFile)) {

            @unlink(static::$pidFile);
        }

        static::$command->info("ProcessServer [" . static::$tcp_name . "] has been stopped");

        if (static::$onMasterStop) {

            call_user_func(static::$onMasterStop);
        }

        exit("By-By");
    }

    /**
     * Parse command.
     *
     * @return void
     * @throws \Exception
     */
    protected static function parseCommand()
    {
        if (static::$_OS !== OS_TYPE_LINUX) {
            return;
        }

        static::$_startFile = getcwd()."/serve.".static::$tcp_name.".loc";

        if (!is_file(static::$_startFile)) {

            file_put_contents(static::$_startFile, "");
        }

        // Get master process PID.
        $master_pid      = is_file(static::$pidFile) ? file_get_contents(static::$pidFile) : 0;

        $master_is_alive = $master_pid && posix_kill($master_pid, 0) && posix_getpid() != $master_pid;

        // Master is still alive?
        if ($master_is_alive) {

            throw new \Exception("ProcessServer already running on behalf of [" . static::$tcp_name . "]");
        }
    }

    /**
     * Display UI
     */
    protected static function monitorWorkersForLinux()
    {
        static::$_status = static::STATUS_RUNNING;

        // Calls signal handlers for pending signals.
        pcntl_signal_dispatch();
        // Suspends execution of the current process until a child has exited, or until a signal is delivered
        $status = 0;
        $pid    = pcntl_wait($status, WUNTRACED);
        // Calls signal handlers for pending signals again.
        pcntl_signal_dispatch();
        // If a child has already exited.
        if ($pid > 0) {
            // Find out witch worker process exited.
            foreach (static::$_pidMap as $worker_id => $worker_pid_array) {
                if (isset($worker_pid_array[$pid])) {
                    $worker = static::$_workers[$worker_id];
                    // Exit status.
                    if ($status !== 0) {
                        static::log("worker[" . $worker->name . ":$pid] exit with status $status");
                    }

                    // For Statistics.
                    if (!isset(static::$_globalStatistics['worker_exit_info'][$worker_id][$status])) {
                        static::$_globalStatistics['worker_exit_info'][$worker_id][$status] = 0;
                    }
                    static::$_globalStatistics['worker_exit_info'][$worker_id][$status]++;

                    // Clear process data.
                    unset(static::$_pidMap[$worker_id][$pid]);

                    // Mark id is available.
                    $id                              = static::getId($worker_id, $pid);
                    static::$_idMap[$worker_id][$id] = 0;

                    break;
                }
            }
            // Is still running state then fork a new worker process.
            if (static::$_status !== static::STATUS_SHUTDOWN) {
                static::forkWorkers();
                // If reloading continue.
                if (isset(static::$_pidsToRestart[$pid])) {
                    unset(static::$_pidsToRestart[$pid]);
                    static::reload();
                }
            }
        }

        // If shutdown state and all child processes exited then master process exit.
        if (static::$_status === static::STATUS_SHUTDOWN && !static::getAllWorkerPids()) {
            static::exitAndClearAll();
        }
    }

    /**
     * Display staring UI.
     *
     * @return void
     */
    protected static function displayUI()
    {

    }

    /**
     * Stop.
     *
     * @return void
     */
    public static function stopAll()
    {
        static::$_status = static::STATUS_SHUTDOWN;

        // For master process.
        if (static::$_masterPid === posix_getpid()) {

            static::$command->info("\n[" . basename(static::$_startFile) . "] stopping ...");

            $worker_pid_array = static::getAllWorkerPids();

            // Send stop signal to all child processes.
            if (static::$_gracefulStop) {

                $sig = SIGTERM;

            } else {

                $sig = SIGINT;
            }

            foreach ($worker_pid_array as $worker_pid) {

                posix_kill($worker_pid, $sig);

                if(!static::$_gracefulStop){

                    Timer::add(static::KILL_WORKER_TIMER_TIME, 'posix_kill', [$worker_pid, SIGKILL], false);
                }
            }
            Timer::add(1, "\\Workerman\\Worker::checkIfChildRunning");

            // Remove statistics file.
            if (is_file(static::$_statisticsFile)) {

                @unlink(static::$_statisticsFile);
            }
        } // For child processes.
        else {

            // Execute exit.
            foreach (static::$_workers as $worker) {

                if(!$worker->stopping){

                    $worker->stop();

                    $worker->stopping = true;
                }
            }
            if (!static::$_gracefulStop || ConnectionInterface::$statistics['connection_count'] <= 0) {

                static::$_workers = [];

                if (static::$globalEvent) {

                    static::$globalEvent->destroy();
                }
                exit(0);
            }
        }
    }
}

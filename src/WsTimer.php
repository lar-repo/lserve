<?php

namespace Lar\LServe;

use Carbon\Carbon;
use Lar\LServe\Server\TcpCommands\TimerRespond;
use Lar\LServe\Server\WebSocketServer;
use Lar\LServe\Traits\TimerGetters;
use Lar\LServe\Traits\TimerMethods;

/**
 * Class WsTimer
 * @package Lar\LServe
 */
abstract class WsTimer {

    use TimerGetters, TimerMethods;

    /**
     * Set an alarm clock for delivery of a signal
     *
     * @var int
     */
    protected $seconds = 1;

    /**
     * Delay run
     *
     * @var Carbon
     */
    protected $delay;

    /**
     * @var int|bool
     */
    protected $timer_id;

    /**
     * @var bool
     */
    protected $persistent = true;

    /**
     * @var int
     */
    protected $iterations = 0;

    /**
     * Maximum number of executions.
     *
     * @var int
     */
    protected $max_executions = 0;

    /**
     * @var Carbon
     */
    protected $die_at;

    /**
     * @var TimerRespond[]
     */
    protected $sends = [];

    /**
     * @param  int  $seconds
     * @return $this
     */
    public function seconds(int $seconds = 1)
    {
        $this->seconds = $seconds < 1 ? 1 : $seconds;

        return $this;
    }

    /**
     * @param  Carbon  $carbon
     * @return $this
     */
    public function delay(Carbon $carbon)
    {
        $this->delay = $carbon;

        return $this;
    }

    /**
     * @param  Carbon  $carbon
     * @return $thiss
     */
    public function dieAt(Carbon $carbon)
    {
        $this->die_at = $carbon;

        return $this;
    }

    /**
     * One execute
     *
     * @return $this
     */
    public function once()
    {
        $this->persistent = false;

        return $this;
    }

    /**
     * @param  int  $executions
     * @return $this
     */
    public function max(int $executions)
    {
        $this->max_executions = $executions;

        return $this;
    }

    /**
     * @param  mixed  ...$props
     * @return $this
     */
    public static function dispatch(...$props)
    {
        $self = new static(...$props);

        LServe::$timers[] = $self;

        return $self;
    }

    /**
     * @param  WebSocketServer  $ws
     * @param $timer_id
     */
    public function tick($timer_id)
    {
        $this->timer_id = $timer_id;

        $this->iterations++;

        if ($this->max_executions && $this->iterations >= $this->max_executions) {

            $this->stop();
        }

        $this->handle();

        foreach ($this->sends as $send) {

            $send->send();
        }

        $this->sends = [];
    }

    /**
     * Execute the timer
     */
    abstract protected function handle(): void;
}
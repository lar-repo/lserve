<?php

namespace Lar\LServe\TcpExecutors;

use Lar\LServe\Server\WebSocketServer;

/**
 * Interface TcpExecutor
 * @package Lar\LServe\TcpExecutors
 */
abstract class TcpExecutor
{
    /**
     * @var WebSocketServer
     */
    protected $ws;

    /**
     * TcpExecutor constructor.
     * @param  WebSocketServer  $ws
     */
    public function __construct(WebSocketServer $ws)
    {
        $this->ws = $ws;
    }

    /**
     * Execute
     */
    abstract public function handle(): void;
}
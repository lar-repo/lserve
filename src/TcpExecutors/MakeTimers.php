<?php

namespace Lar\LServe\TcpExecutors;

use Lar\LServe\Server\WebSocketServer;
use Lar\LServe\WsTimer;
use Workerman\Lib\Timer;

class MakeTimers extends TcpExecutor
{

    public function handle(): void
    {
        $timers = \Cache::pull('ws:timers');

        if (is_array($timers)) {

            foreach ($timers as $timer) {

                /** @var WsTimer $timer */
                $timer = unserialize($timer);

                if ($timer instanceof WsTimer) {

                    if ($timer->getDelay()) {

                        $this->delayTimer($timer);
                    }

                    else {

                        $this->makeTimer($timer);
                    }
                }
            }
        }

        else if (is_string($timers)) {

            /** @var WsTimer $timer */
            $timer = unserialize($timers);

            if ($timer instanceof WsTimer) {

                if ($timer->getDelay()) {

                    $this->delayTimer($timer);
                }

                else {

                    $this->makeTimer($timer);
                }
            }
        }
    }

    /**
     * Make timer
     * @param  WsTimer  $timer
     */
    public function makeTimer(WsTimer $timer)
    {
        WebSocketServer::info(["Running timer: [".get_class($timer)."] with [".$timer->getSeconds()." sec interval]"]);
        $timer_id = Timer::add($timer->getSeconds(), [$timer, 'tick'], [&$timer_id], $timer->getPersistent());
        if ($timer->getDieAt() && $timer->getPersistent()) {
            Timer::add(now()->diffInSeconds($timer->getDieAt()), [$timer, 'stop'], [], false);
        }
    }

    /**
     * Delay timer
     * @param  WsTimer  $timer
     */
    protected function delayTimer(WsTimer $timer)
    {
        $sec = now()->diffInSeconds($timer->getDelay());
        WebSocketServer::info(["Delayed timer: [".get_class($timer)."] on [{$sec} sec]"]);
        Timer::add($sec, [$this, 'makeTimer'], [$timer], false);
    }
}
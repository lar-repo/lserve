<?php

namespace Lar\LServe\DevExecutor;

use Lar\LServe\Executor;
use Lar\LServe\Server\WebSocketServer;

/**
 * Dev Class
 * 
 * @package App\Http\WsExecutors
 */
class DevExecutor extends Executor
{
    /**
     * Public variable Name
     * 
     * @var string
     */
    public $name = "dev";

    /**
     * __invoke Public method
     * 
     * @return void
     */
    public function __invoke() {
        
        WebSocketServer::info('My Executor [Dev]');
        
        return ;
    }

    public function show_clients()
    {
        $this->sender->send(["console.log" => WebSocketServer::$clients], true);
    }
}

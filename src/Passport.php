<?php

namespace Lar\LServe;

use Illuminate\Auth\SessionGuard;
use Illuminate\Database\Eloquent\Model;
use Lar\LServe\Server\TcpCommands\WebGroup;
use Lar\LServe\Server\TcpSender;
use Lar\LServe\Server\WebSocketServer;
use Symfony\Component\Console\Exception\LogicException;

class Passport
{
    /**
     * My ID
     *
     * @var string
     */
    protected static $id;

    /**
     * I am a have groups
     *
     * @var array
     */
    protected static $groups = [];

    /**
     * User identifications
     *
     * @var array
     */
    protected static $ids = [];

    /**
     * Identified guards. Array of SessionGuard
     *
     * @var array | SessionGuard
     */
    protected static $guard = [];

    /**
     * Check registered process
     *
     * @return bool
     */
    public static function isRegistered()
    {
        return static::$id ? true : false;
    }

    /**
     * My system ID
     *
     * @return string
     */
    public static function getMyId()
    {
        if (!static::$id) {
            static::$id = WebSocketServer::identifyValue(session()->getId());
        }

        return static::$id;
    }

    /**
     * Add group in to my passport
     *
     * @param string|array $group
     * @return bool|int
     * @throws \Exception
     */
    public static function enterGroup($group)
    {
        if (is_array($group)) {
            $group = func_get_args();
        }

        return (new TcpSender())->setRecipient(WebGroup::class, static::getMyId())->send(["+" => $group]);
    }

    /**
     * Leave the group
     *
     * @param string|array $group
     * @return bool
     * @throws \Exception
     */
    public static function leaveGroup($group)
    {
        if (is_array($group)) {
            $group = func_get_args();
        }

        return (new TcpSender())->setRecipient(WebGroup::class, static::getMyId())->send(["-" => $group]);
    }

    /**
     * Add identification in to passport
     *
     * @param $key
     * @param $value
     * @throws \Exception
     */
    public static function addIdentification($key, $value)
    {
        if (strtoupper($key) == 'id') {

            throw new \Exception("This id name is already taken.");
        }

        static::$ids[strtoupper($key)] = $value;
    }

    /**
     * Add guard in to identifier
     *
     * @param string $guard
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    public static function addGuardIdentification($guard = "web")
    {
        static::$guard[$guard] = \Auth::guard($guard);

        return static::$guard[$guard];
    }

    /**
     * Guard list getter
     *
     * @return array
     */
    public static function getGuardList()
    {
        return array_keys(static::$guard);
    }

    /**
     * IDS Getter
     *
     * @return array
     * @throws \Exception
     */
    public static function getIds()
    {
        foreach (static::$guard as $key => $item) {

            $model = $item->user();

            if ($model instanceof Model) {

                $ids = ['id'];

                if (isset($model->ids)) {

                    $ids = array_merge($model->ids);
                }

                foreach ($ids as $value) {

                    static::addIdentification($key . '_' . $value, $model->{$value});
                }
            }
        }

        return static::$ids;
    }
}

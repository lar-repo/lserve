<?php

namespace Lar\LServe\Model;

use Lar\LServe\Passport;

trait LSModel
{
    /**
     * LSModel constructor.
     */
    public function __construct()
    {
        Passport::addModelIdentification($this);
    }
}

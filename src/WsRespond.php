<?php

namespace Lar\LServe;

use Lar\Layout\Respond;

/**
 * Class WsRespond
 * @package Lar\LServe
 */
class WsRespond extends Respond {

    /**
     * @var bool|string
     */
    protected $person = [];

    /**
     * @var bool
     */
    protected $conn = false;

    /**
     * @var array
     */
    protected $conditions = [];

    /**
     * @var bool
     */
    protected $sent = false;

    /**
     * @param  string|null  $event
     * @param $data
     * @return $this
     */
    public function emit(string $event = null, $data = null)
    {
        if ($event) {

            $this->insert($event, $data);

            $this->with($event, $event);
        }


        return $this;
    }

    /**
     * @param $eq
     * @param  string  $event
     * @param  null  $data
     * @return $this
     */
    public function emitIf($eq, string $event = null, $data = null)
    {
        if ($eq) {

            $this->emit($event, $data);
        }

        return $this;
    }

    /**
     * @param  string  $name
     * @param $value
     * @return $this
     */
    public function with(string $name = null, $value = null)
    {
        if ($name) {

            $this->conditions[$name] = $value;
        }

        return $this;
    }

    /**
     * @param  array  $conditions
     * @return $this
     */
    public function conditions(array $conditions)
    {
        $this->conditions = array_merge($this->conditions, $conditions);

        return $this;
    }

    /**
     * @param  string|array  $connection_id
     * @return $this
     */
    public function to($connection_id)
    {
        if (is_array($connection_id)) {

            $this->person = array_merge($this->person, $connection_id);
        }

        else {

            $this->person[] = $connection_id;
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function self()
    {
        $this->person[] = Passport::getMyId();

        return $this;
    }

    /**
     * @return $this
     */
    public function current()
    {
        $this->conn = true;

        return $this;
    }

    /**
     * Destruct sender
     * @throws \Exception
     */
    public function __destruct()
    {
        $this->send();
    }

    /**
     * @throws \Exception
     */
    public function send()
    {
        if (!$this->sent) {

            if ($this->person) {

                $this->with('PERSON_ID', $this->person);
            }

            send_data($this->items, array_merge($this->conditions, ['CONN_ID' => $this->conn]));

            $this->sent = true;
        }
    }

    /**
     * @return static
     */
    public static function create()
    {
        return new static;
    }
}
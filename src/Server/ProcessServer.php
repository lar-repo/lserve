<?php

namespace Lar\LServe\Server;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Workerman\Connection\ConnectionInterface;
use Workerman\Lib\Timer;
use Workerman\Worker;

/**
 * Class ProcessServer
 *
 * @package Liv\Server
 * @method onWorkerStart(\Closure $closure) Emitted when worker processes start.
 * @method onConnect(\Closure $closure) Emitted when a socket connection is successfully established.
 * @method onMessage(\Closure $closure) Emitted when data is received.
 * @method onClose(\Closure $closure) Emitted when the other end of the socket sends a FIN packet.
 * @method onError(\Closure $closure) Emitted when an error occurs with connection.
 * @method onBufferFull(\Closure $closure) Emitted when the send buffer becomes full.
 * @method onBufferDrain(\Closure $closure) Emitted when the send buffer becomes empty.
 * @method onWorkerStop(\Closure $closure) Emitted when worker processes stoped.
 * @method onWorkerReload(\Closure $closure) Emitted when worker processes get reload signal.
 */
class ProcessServer extends Worker
{
    /**
     * Parent command object
     *
     * @var Command
     */
    protected static $command;

    /**
     * Original signature
     *
     * @var string
     */
    public static $original_signature;

    /**
     * Init timestamp
     *
     * @var int
     */
    public $time;

    /**
     * ProcessServer constructor.
     *
     * @param string $socket_name
     * @param array $context_option
     * @throws \Exception
     */
    public function __construct(string $socket_name, array $context_option = [])
    {
        if (!static::$command instanceof Command)
            throw new \Exception("It is necessary to transfer a class object to the Symfony team. Use \"ProcessServer::commandInit(...)\"");

        $this->time = time();

        parent::__construct($socket_name, $context_option);
    }

    /**
     * Command setter
     *
     * @param Command $command
     */
    public static function commandInit(Command &$command) {

        static::$command = $command;
    }

    /**
     * Magic method
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if (substr($name, 0, 2) == "on" && property_exists($this, $name)) {

            if (isset($arguments[0]) && $arguments[0] instanceof \Closure) {

                $this->{$name} = $arguments[0];

                return $this;

            } else
                throw new \Exception("No \"Closure\" parameter specified.");
        }


    }

    /**
     * Exit current process.
     *
     * @return void
     */
    protected static function exitAndClearAll()
    {
        foreach (static::$_workers as $worker) {
            $socket_name = $worker->getSocketName();
            if ($worker->transport === 'unix' && $socket_name) {
                list(, $address) = explode(':', $socket_name, 2);
                @unlink($address);
            }
        }
        if (is_file(static::$pidFile)) @unlink(static::$pidFile);
        static::info("ProcessServer [" . static::$original_signature . "] has been stopped");
        if (static::$onMasterStop) {
            call_user_func(static::$onMasterStop);
        }
        exit("By-By");
    }

    /**
     * Parse command.
     *
     * @return void
     * @throws \Exception
     */
    protected static function parseCommand()
    {
        if (static::$_OS !== OS_TYPE_LINUX) {
            return;
        }

        static::$_startFile = getcwd()."/serve.".static::$original_signature.".loc";

        if (!is_file(static::$_startFile)) {

            file_put_contents(static::$_startFile, "");
        }

        // Get master process PID.
        $master_pid      = is_file(static::$pidFile) ? file_get_contents(static::$pidFile) : 0;
        $master_is_alive = $master_pid && posix_kill($master_pid, 0) && posix_getpid() != $master_pid;


        // Master is still alive?
        if ($master_is_alive) {

            static::error("ProcessServer already running on behalf of [" . static::$original_signature . "]");
            throw new \Exception("ProcessServer already running on behalf of [" . static::$original_signature . "]");
        }
    }

    /**
     * Display UI
     */
    protected static function monitorWorkersForLinux()
    {
        static::$_status = static::STATUS_RUNNING;

        // Calls signal handlers for pending signals.
        pcntl_signal_dispatch();
        // Suspends execution of the current process until a child has exited, or until a signal is delivered
        $status = 0;
        $pid    = pcntl_wait($status, WUNTRACED);
        // Calls signal handlers for pending signals again.
        pcntl_signal_dispatch();
        // If a child has already exited.
        if ($pid > 0) {
            // Find out witch worker process exited.
            foreach (static::$_pidMap as $worker_id => $worker_pid_array) {
                if (isset($worker_pid_array[$pid])) {
                    $worker = static::$_workers[$worker_id];
                    // Exit status.
                    if ($status !== 0) {
                        static::log("worker[" . $worker->name . ":$pid] exit with status $status");
                    }

                    // For Statistics.
                    if (!isset(static::$_globalStatistics['worker_exit_info'][$worker_id][$status])) {
                        static::$_globalStatistics['worker_exit_info'][$worker_id][$status] = 0;
                    }
                    static::$_globalStatistics['worker_exit_info'][$worker_id][$status]++;

                    // Clear process data.
                    unset(static::$_pidMap[$worker_id][$pid]);

                    // Mark id is available.
                    $id                              = static::getId($worker_id, $pid);
                    static::$_idMap[$worker_id][$id] = 0;

                    break;
                }
            }
            // Is still running state then fork a new worker process.
            if (static::$_status !== static::STATUS_SHUTDOWN) {
                static::forkWorkers();
                // If reloading continue.
                if (isset(static::$_pidsToRestart[$pid])) {
                    unset(static::$_pidsToRestart[$pid]);
                    static::reload();
                }
            }
        }

        // If shutdown state and all child processes exited then master process exit.
        if (static::$_status === static::STATUS_SHUTDOWN && !static::getAllWorkerPids()) {
            static::exitAndClearAll();
        }
    }

    /**
     * Display staring UI.
     *
     * @return void
     */
    protected static function displayUI()
    {
        static::$command->table(["Process Server name", "PHP Version"], [[env("APP_NAME"), PHP_VERSION]]);

        if (static::$command->option('debug')) {

            $list_headers = [];

            foreach(static::getUiColumns() as $column_name => $prop){
                $column_name == 'socket' && $column_name = 'listen';
                $list_headers[] = ucfirst($column_name);
            }

            $list_rows = [];

            $iteration = 0;
            foreach (static::$_workers as $worker) {
                foreach(static::getUiColumns() as $column_name => $prop){

                    $list_rows[$iteration][] = strip_tags($worker->{$prop});
                }

                $iteration++;
            }

            static::$command->table($list_headers, $list_rows);
        }

        static::$command->info("Press Ctrl+C to stop. Start success.\n");
    }

    /**
     * Stop.
     *
     * @return void
     */
    public static function stopAll()
    {
        static::$_status = static::STATUS_SHUTDOWN;
        // For master process.
        if (static::$_masterPid === posix_getpid()) {
            static::info("\n[" . basename(static::$_startFile) . "] stopping ...");
            $worker_pid_array = static::getAllWorkerPids();
            // Send stop signal to all child processes.
            if (static::$_gracefulStop) {
                $sig = SIGTERM;
            } else {
                $sig = SIGINT;
            }
            foreach ($worker_pid_array as $worker_pid) {
                posix_kill($worker_pid, $sig);
                if(!static::$_gracefulStop){
                    Timer::add(static::KILL_WORKER_TIMER_TIME, 'posix_kill', [$worker_pid, SIGKILL], false);
                }
            }
            Timer::add(1, "\\Workerman\\Worker::checkIfChildRunning");
            // Remove statistics file.
            if (is_file(static::$_statisticsFile)) {
                @unlink(static::$_statisticsFile);
            }
        } // For child processes.
        else {
            // Execute exit.
            foreach (static::$_workers as $worker) {
                if(!$worker->stopping){
                    $worker->stop();
                    $worker->stopping = true;
                }
            }
            if (!static::$_gracefulStop || ConnectionInterface::$statistics['connection_count'] <= 0) {
                static::$_workers = [];
                if (static::$globalEvent) {
                    static::$globalEvent->destroy();
                }
                exit(0);
            }
        }
    }

    /**
     * Send info
     *
     * @param $info
     * @param null $arr
     */
    public static function info($info, $arr = null)
    {
        if (!is_array($info)) {
            $info = func_get_args();
        }

        foreach ($info as $item)
            static::informer("info", $item, $arr);
    }

    /**
     * Send comment
     *
     * @param $comment
     * @param null $arr
     */
    public static function comment($comment, $arr = null)
    {
        if (!is_array($comment)) {
            $comment = func_get_args();
        }

        foreach ($comment as $item)
            static::informer("comment", $item, $arr);
    }

    /**
     * Send question
     *
     * @param $question
     * @param null $arr
     */
    public static function question($question, $arr = null)
    {
        if (!is_array($question)) {
            $question = func_get_args();
        }

        foreach ($question as $item)
            static::informer("question", $item, $arr);
    }

    /**
     * Send error
     *
     * @param $error
     * @param array $arr
     */
    public static function error($error, $arr = null)
    {
        if (!is_array($error)) {
            $error = func_get_args();
        }

        foreach ($error as $item)
            static::informer("error", $item, $arr);
    }

    /**
     * Send line
     *
     * @param $line
     * @param array $arr
     */
    public static function line($line, $arr = null)
    {
        if (!is_array($line)) {
            $line = func_get_args();
        }

        foreach ($line as $item)
            static::informer("line", $item, $arr);
    }

    /**
     * Informer
     *
     * @param $type
     * @param $message
     * @param null $arr
     */
    protected static function informer($type, $message, $arr = null)
    {
        if (!\App::isLocal() && $type !== 'error') {
            return ;
        }

        if (is_array($arr)) $arr = var_export_array($arr, true, 150);
        else $arr = "";

        if (WebSocketServer::$clients instanceof Collection) {
            $count = WebSocketServer::$clients->count();
        } else {
            $count = 0;
        }

        if (!is_array($message)) {

            $line = "[".date("H:i:s")."][CONN_COUNT:{$count}]".$message.(is_string($arr) ? $arr : "");
        }

        if (static::$command->option('debug') && !is_array($message)) {
            static::$command->{$type}($line);
        }

        if (!static::$command->option('debug') && !is_array($message) && ($type === "error" || $type === "info")) {
            static::$command->{$type}("[".date("d.m.Y H:i:s")."][".strtoupper($type)."] ".$message.(is_string($arr) ? $arr : ""));
        }

        if (!is_array($message)) {

            static::toConsole($type, isset($line) ? $line : $message);
        }
    }

    /**
     * @param $type
     * @param $message
     */
    protected static function toConsole($type, $message)
    {
        if (WebSocketServer::$clients && WebSocketServer::$clients->has("CONSOLE")) {

            WebSocketServer::$clients->get("CONSOLE")->send(["serve_log" => ["type" => $type, "message" => $message]], false);
        }
    }
}

<?php

namespace Lar\LServe\Server\TcpCommands;

use Illuminate\Support\Collection;
use Lar\LServe\Server\WebSocketServer;

class WebGroup implements TcpCommandInterface
{

    /**
     * TcpCommand constructor.
     *
     * @param array $props Filter settings to find the desired connection.
     * @param array $data Data to send to the connection.
     */
    public function __construct(array $props = [], array $data = [])
    {
        if (isset($props[0]) && WebSocketServer::$clients->has($props[0])) {

            $connection = WebSocketServer::$clients->get($props[0]);

            if (!isset($connection["GROUPS"]))
                $connection["GROUPS"] = new Collection();

            foreach ($data as $key => $group) {

                if ($key == "+") {
                    if (is_array($group)){

                        foreach ($group as $item) {

                            $connection["GROUPS"]->put($group, (string)$item);
                        }

                    } else
                        $connection["GROUPS"]->put($group, (string)$group);
                }

                if ($key == "-") {
                    $connection["GROUPS"]->except($group);
                }
            }

            WebSocketServer::$clients->put($props[0], $connection);

            WebSocketServer::comment(" WebGroup [{$props[0]}]", $connection["GROUPS"]->values()->toArray());
        }
    }
}

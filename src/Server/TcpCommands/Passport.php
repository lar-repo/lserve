<?php

namespace Lar\LServe\Server\TcpCommands;

use Illuminate\Support\Collection;
use Lar\LServe\Server\ConnectionWrapper;
use Lar\LServe\Server\WebSocketServer;

class Passport implements TcpCommandInterface
{

    /**
     * Passport constructor.
     *
     * @param array $props Filter settings to find the desired connection.
     * @param array $data Data to send to the connection.
     */
    public function __construct(array $props = [], array $data = [])
    {
        $new = true;

        if (WebSocketServer::$clients->has($data["REGISTRATION"])) {

            $connection = WebSocketServer::$clients->get($data["REGISTRATION"]);
            $new = false;

        } else {

            $connection = new ConnectionWrapper();
        }


        $connection["ID"] = $data["REGISTRATION"];

        $connection["GUARDED"] = isset($connection["GUARDED"]) ? $connection["GUARDED"] : 0;
        $connection["GUARDS"] = isset($connection["GUARDS"]) ? $connection["GUARDS"] : null;

        if (isset($data["IDS"]) && is_array($data["IDS"])) {

            $connection["GUARDED"] = count($data["IDS"]);

            foreach ($data["IDS"] as $KEY => $ID) {

                $connection[$KEY] = $ID;
            }
        }

        if (isset($data["GUARDS"])) {

            $connection["GUARDS"] = $data["GUARDS"];

        }

        if (!isset($connection["QUEUE"])) {

            $connection["QUEUE"] = new Collection();
        }
        
        if (isset($data['QUEUE']) && is_array($data['QUEUE'])) {

            $connection["QUEUE"] = $connection["QUEUE"]->merge($data['QUEUE']);
        }

        $connection["TIMESTAMP"] = time();
        $connection["DATE"] = now();
        $connection["EXECUTORS"] = isset($connection["EXECUTORS"]) ? $connection["EXECUTORS"] : [];

        if (isset($data["EXECUTORS"]) && is_array($data["EXECUTORS"])) {

            foreach ($data["EXECUTORS"] as $key => $item) {

                if (!class_exists($item)) {

                    WebSocketServer::error("Execute class [{$key}] => [{$item}] not exists!");

                } else {

                    $connection["EXECUTORS"][$key] = $item;
                }
            }
        }

        WebSocketServer::$clients->put($data["REGISTRATION"], $connection);
        WebSocketServer::online($connection["ID"]);

        if ($connection["GUARDS"]) {

            $guards = strtoupper($connection["GUARDS"]);

            if ($new) {

                WebSocketServer::comment("Register guard [{$guards}] ID [{$connection["ID"]}]");

            } else {

                WebSocketServer::comment("Restored guard [{$guards}] ID [{$connection["ID"]}]");
            }

        } else {

            if ($new) {

                WebSocketServer::comment("New registration [{$connection["ID"]}]");

            } else {

                WebSocketServer::comment("Restored registration [{$connection["ID"]}]");
            }
        }
    }
}

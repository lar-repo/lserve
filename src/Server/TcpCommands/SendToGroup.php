<?php

namespace Lar\LServe\Server\TcpCommands;

use Lar\LServe\Server\WebSocketServer;

class SendToGroup implements TcpCommandInterface
{
    /**
     * Send Group list
     *
     * @var array
     */
    protected $props;

    /**
     * TcpCommand constructor.
     *
     * @param array $props Filter settings to find the desired connection.
     * @param array $data Data to send to the connection.
     */
    public function __construct(array $props = [], array $data = [])
    {
        $this->props = array_values($props);
    }

    public function send($data)
    {
        WebSocketServer::$clients->where("GROUPS", "!=", "")->map(function ($item) use ($data){

            foreach ($this->props as $prop) {

                if($item["GROUPS"]->has($prop)) {

                    $item->send($data);
                }
            }

        });
    }
}

<?php

namespace Lar\LServe\Server\TcpCommands;

use Lar\LServe\Server\ConnectionWrapper;
use Lar\LServe\Server\WebSocketServer;
use Lar\LServe\WsRespond;

/**
 * Class TimerRespond
 * @package Lar\LServe\Server\TcpCommands
 */
class TimerRespond extends WsRespond
{
    /**
     * Diff send
     */
    public function send()
    {
        $clients = WebSocketServer::$clients;

        if ($this->person) {

            if (is_array($this->person)) {

                $clients = $clients->whereIn("ID", $this->person);
            }

            else {

                $clients = $clients->where("ID", $this->person);
            }
        }

        foreach ($this->conditions as $key => $condition) {

            if (is_array($condition)) {

                $clients = $clients->whereIn(strtoupper($key), $condition);

            } else {

                $clients = $clients->where(strtoupper($key), $condition);
            }
        }

        $clients->map(function ($connection) {

            /** @var ConnectionWrapper $connection */
            $connection["TIMESTAMP"] = time();
            $connection->send($this->items, $this->conn);
        });
    }

    /**
     * Destruct off
     */
    public function __destruct()
    {
    }
}
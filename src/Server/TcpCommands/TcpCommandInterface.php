<?php

namespace Lar\LServe\Server\TcpCommands;

use Lar\LServe\Server\ConnectionWrapper;

/**
 * Interface TcpCommandInterface
 *
 * @package Lar\LServe\Server\TcpCommands
 */
interface TcpCommandInterface
{
    /**
     * TcpCommand constructor.
     *
     * @param array $props Filter settings to find the desired connection.
     * @param array $data Data to send to the connection.
     */
    public function __construct(array $props = [], array $data = []);
}

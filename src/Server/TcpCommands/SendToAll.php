<?php

namespace Lar\LServe\Server\TcpCommands;

use Lar\LServe\Server\ConnectionWrapper;
use Lar\LServe\Server\WebSocketServer;

class SendToAll implements TcpCommandInterface
{

    /**
     * TcpCommand constructor.
     *
     * @param array $props Filter settings to find the desired connection.
     * @param array $data Data to send to the connection.
     */
    public function __construct(array $props = [], array $data = [])
    {

    }

    public function send(array $data, array $conditions)
    {
        WebSocketServer::$clients->map(function ($connection) use ($data) {

            /** @var ConnectionWrapper $connection */
            $connection->send($data);
        });
    }
}

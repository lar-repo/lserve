<?php

namespace Lar\LServe\Server;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Lar\LServe\Executor;
use Lar\LServe\LServe;
use Lar\LServe\Server\TcpCommands\Passport;
use Lar\LServe\Server\TcpCommands\TcpCommandInterface;
use Lar\LServe\TcpExecutors\TcpExecutor;
use function Psy\debug;
use Workerman\Connection\AsyncTcpConnection;

class WebSocketServer extends ProcessServer
{

    /**
     * Passport states
     */
    const I_AM_ANONYMOUS = "__I_AM_ANONYMOUS",
        I_AM_IDENTIFIED_USER = "__I_AM_IDENTIFIED_USER",
        I_AM_IN_A_GROUP = "__I_AM_IN_A_GROUP",
        GET_INIT = "GET_INIT";

    /**
     * Connected clients
     *
     * @var Collection
     */
    public static $clients;

    public static $binance;

    public static $ws_cache = "ws:online";

    /**
     * WebSocketServer constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $context = [];

        if (config("lserve.ssl_enable", false) && config("lserve.ssl_config", [])) {

            $context = ['ssl' => config("lserve.ssl_config", [])];

            $this->transport = 'ssl';
        }

        \Cache::forever(static::$ws_cache, []);

        parent::__construct(static::getLink(), $context);

        $this->connectionInit();

        $this->closeInit();
    }

    /**
     * @param  string  $id
     * @return bool
     */
    static function isOnline (string $id) {

        return isset(static::onlineList()[$id]);
    }

    /**
     * Protocol link
     *
     * @param array $params
     * @return string
     */
    public static function getLink(array $params = []){

        $ip = "0.0.0.0";
        $port = env("PROSERVER_PORT", 4060);

        return "websocket://{$ip}:{$port}".(count($params) ? "?".http_build_query($params) : "");
    }

    /**
     * Protocol link
     *
     * @param array $params
     * @return string
     */
    public static function getPublicLink(array $params = []){

        $ip = env("PROSERVER_PUBLIC", gethostbyname(gethostname()));

        $port = env("PROSERVER_PORT", 4060);
        $prot = "ws";

        if (preg_match('/^https.*/', env('APP_URL'))) {

            $prot .= "s";
        }

        return "{$prot}://{$ip}:{$port}".(count($params) ? "?".http_build_query($params) : "");
    }

    /**
     * Run "start" and "message" events
     */
    public function runWithTcp()
    {
        $this->onWorkerStart(function () {

            ProcessTimer::cleaner();
            ProcessTimer::custom();

            (new TcpServer())->onMessage(function ($connection, $data) {

                if (strlen($data) < 255 && $class = config('lserve.tcp_executors.'.$data, false)) {

                    if (class_exists($class)) {

                        return (new $class($this))->handle();
                    }
                }

                $data = array_diff(explode(PHP_EOL, $data), array(null));

                try {

                    if (is_string($data) && \Cache::has($data)) {

                        $data = json_decode(\Cache::pull($data), true);
                    }

                    foreach ($data as $item) {

                        if (is_string($item) && \Cache::has($item)) {

                            $item = json_decode(\Cache::pull($item), true);
                        }

                        else {

                            $item = json_decode($item, true);
                        }

                        if (!is_array($item)) {

                            continue ;
                        }

                        foreach ($item as $key => $datum) {

                            if (strpos($key, "\\") !== false) {

                                unset($item[$key]);

                                $conditions = [];

                                if (isset($item["__CONDITIONS__"])) {

                                    $conditions = $item["__CONDITIONS__"];
                                    unset($item["__CONDITIONS__"]);
                                }

                                $object = new $key($datum, $item);

                                if (!$object instanceof TcpCommandInterface) {

                                    static::error("It is not recommended to use non-special classes. It is necessary to apply an interface to the class \"TcpCommandInterface\".");
                                }

                                if (method_exists($object, "send")) {

                                    $object->send($item, $conditions);

                                    if (count($item)) {

                                        static::comment("Send to " . var_export_array($conditions, true, 255) . " => ", $item);
                                    }
                                }
                            }
                        }
                    }

                } catch (\Exception $exception) {}

            })->listen();
        });

        $this->onMessage(function ($connection, $data) {

            $connection->send(json_encode(['server_time' => time()]));

            if ($data === md5(env("APP_KEY"))) {

                $data = "CONSOLE";
            }
            
            if (strlen($data) < 255 && static::$clients->has($data)) {

                $item = static::$clients->get($data);

                /** @var ConnectionWrapper $item */
                if ($item->getConnection($connection)) {

                    $mess = $item["CURRENT_CONNECTION_ID"] != $connection->id;

                    $item->setConnection($connection);

                    if ($mess) {

                        static::comment("Change current ID Connection to: [{$connection->id}]");
                    }

                } else {

                    static::error("No user definition found! [Level 4]");
                    $connection->close();
                }

            } else {

                try {

                    $data = json_decode($data, true);

                    if (isset($data['EMIT']) && isset($data['CLIENT']) && static::$clients->has($data['CLIENT'])) {

                        $item = static::$clients->get($data['CLIENT']);

                        if (isset($data['ON']) && $data['ON'] && $item->getConnection($connection)) {

                            $item[strtoupper($data['EMIT'])] = $data['EMIT'];
                            static::info("CLIENT [{$data['CLIENT']}] ON: " . $data['EMIT']);

                        }

                        else if (isset($data['OFF']) && $data['OFF'] && $item->getConnection($connection)) {

                            if (isset($item[strtoupper($data['EMIT'])])) {
                                static::info("CLIENT [{$data['CLIENT']}] OFF: " . $data['EMIT']);
                                unset($item[strtoupper($data['EMIT'])]);
                            }
                        }
                    }

                    else if (isset($data["ID"]) && isset($data["EXECUTE"]) && strlen($data["EXECUTE"]) < 255) {

                        if (!isset($data["DATA"])) {

                            $data["DATA"] = null;
                        }

                        if ($data["ID"] === md5(env("APP_KEY"))) {

                            $data["ID"] = "CONSOLE";
                        }


                        if (static::$clients->has($data["ID"])) {

                            $client = static::$clients->get($data["ID"]);

                            $list = config("lserve.executors", []);

                            $list = array_merge($list, config('ws_executors', []));

                            if (static::$command->option("dev")) {

                                $list = array_merge($list, include __DIR__ . "/../dev_ws_executors.php");
                            }

                            $list = array_merge($list, $client["EXECUTORS"]);

                            if (!isset($data["METHOD"])) {

                                $exec_test = explode("@", $data["EXECUTE"]);
                                $exec_count = count($exec_test);

                                if ($exec_count == 2) {

                                    $data["METHOD"] = $exec_test[1];
                                    $data["EXECUTE"] = $exec_test[0];

                                } else if ($exec_count > 2) {

                                    $data["EXECUTE"] = $exec_test[0];
                                    unset($exec_test[0]);
                                    $data["METHOD"] = $exec_test;
                                }
                            }

                            $executor = isset($list[$data["EXECUTE"]]) ? $list[$data["EXECUTE"]] : null;

                            if (!$executor) {

                                if (LServe::$executors->has($data["EXECUTE"])) {

                                    $executor = LServe::$executors->get($data["EXECUTE"]);

                                } else if (class_exists($data["EXECUTE"])) {

                                    $executor = $data["EXECUTE"];

                                } else {

                                    $test = base64_decode($data["EXECUTE"]);

                                    $test = sslDec($test);

                                    if (preg_match('/^[A-Za-z0-9\\\\]+$/', $test)) {

                                        $executor = $test;
                                    }
                                }
                            }

                            if (is_string($executor)) {

                                $comment = "Execute: [{$data["ID"]}] => [{$executor}]";
                                $exec = new $executor();

                                if ($exec instanceof Executor) {

                                    $exec->setSender($client)
                                        ->setData($data["DATA"])
                                        ->setSenderId($data["ID"]);

                                    if ($exec->isValid()) {

                                        if (isset($data["METHOD"])) {

                                            if (is_string($data["METHOD"])) {

                                                if (method_exists($exec, $data["METHOD"]) && !preg_match('/^__[\-\_a-z]*$/', $data["METHOD"])) {

                                                    embedded_call([$exec, $data["METHOD"]], $data["DATA"] ? $data["DATA"] : []);
                                                    $comment .= " => [{$data["METHOD"]}]";

                                                } else {

                                                    static::error("An attempt to call the nonexistent method [{$data["METHOD"]}] in [{$executor}].");
                                                }
                                            }

                                            if (is_array($data["METHOD"])) {

                                                foreach ($data["METHOD"] as $method) {

                                                    if (method_exists($exec, $method) && !preg_match('/^__[\-\_a-z]*$/', $method)) {

                                                        embedded_call([$exec, $method], $data["DATA"] ? $data["DATA"] : []);
                                                        $comment .= " => [{$method}]";

                                                    } else {

                                                        static::error("An attempt to call the nonexistent method [{$method}] in [{$executor}].");
                                                    }
                                                }
                                            }

                                        } else {

                                            if (method_exists($exec, "__invoke")) {

                                                /** @var \Closure $exec */
                                                embedded_call([$exec, "__invoke"], $data["DATA"] ? $data["DATA"] : []);

                                            } else {

                                                static::error("When calling the executor, no method was called. [{$executor}]");
                                            }
                                        }

                                        if ($exec->respond->count()) {
                                            /** @var Executor $exec */
                                            $exec->sender->send(json_decode($exec->respond->toJson(),1));
                                        }

                                    } else {

                                        static::error("Executor [{$executor}] not valid! [Level 10]");
                                        $connection->close();
                                    }


                                    static::comment($comment);

                                } else {

                                    static::error("Executor is not true! [Level 9]");
                                    $connection->close();
                                }

                            } else {

                                static::error("No executor found! [Level 8]");
                                $connection->close();
                            }

                        } else {

                            static::error("No user definition found! [Level 7]");
                            $connection->close();
                        }

                    } else {

                        static::error("Bead Request! [Level 6]");
                        $connection->close();
                    }

                } catch (\Exception $e) {

                    static::error($e->getMessage() . ": line " . $e->getLine());
                    static::error("Bead Request! [Level 5]");
                    $connection->close();
                }
            }
        });

        static::runAll();
    }

    /**
     * Secure Identify command index value.
     *
     * @param string|null $salt
     * @return string
     */
    public static function identifyValue(string $salt = ""){

        return md5(env("APP_NAME") . " ^ " . env("APP_KEY") . " ^ " . $salt);
    }

    /**
     * Close initialization
     */
    private function closeInit(){

        $this->onClose(function ($connection) {
            static::$clients->map(function ($item, $key) use ($connection) {

                /** @var ConnectionWrapper $item */
                if ($item->removeConnection($connection)) {
                    static::comment("Disconnect point: [{$item["ID"]}]");
                }
            });
        });
    }

    /**
     * Connection initialization
     */
    private function connectionInit(){

        if (!static::$clients) {

            static::$clients = new Collection();
        }

        $this->onConnect(function ($connection) {

            $connection->onWebSocketConnect = function ($connection) {

                $request = new Request($_GET);

                $query = [];
                parse_str($_SERVER["QUERY_STRING"], $query);

                if ($_SERVER["HTTP_ORIGIN"] == "file://" &&  isset($query["console"]) && $query["console"] == md5(env("APP_KEY"))) {

                    $connection_wrapper = new ConnectionWrapper();
                    $connection_wrapper->setConnection($connection);
                    $connection_wrapper["ID"] = "CONSOLE";
                    $connection_wrapper["IMMORTAL"] = true;

                    static::$clients->put("CONSOLE", $connection_wrapper);

                    new Passport([], ["REGISTRATION" => "CONSOLE"]);

                    $connection_wrapper->queue();

                    static::comment("Console included!");

                } else {

                    //if (trim($_SERVER["HTTP_ORIGIN"], "/") != trim(env("APP_URL"), "/")) {
                    if (false) {

                        static::error("No user definition found! [Level 1]");
                        $connection->close();

                    } else {

                        if ($request->has(static::identifyValue())) {

                            if (static::$clients->where("ID", $request->get(static::identifyValue()))->first()) {

                                static::$clients->where("ID", $request->get(static::identifyValue()))->map(function ($item) use ($connection, $request) {

                                    if (($item["TIMESTAMP"] + 30) < time()) {
                                        static::error("No user definition found! [Level 4][Timeout 30 sec]");
                                        return $item;
                                    }

                                    /** @var ConnectionWrapper $item */
                                    $item->setConnection($connection);
                                    $item->queue();
                                    static::comment("Set ID Connection: [{$connection->id}]");
                                    $connection->send(json_encode(['registered' => 'registered']));
                                    return $item;
                                });

                            } else {

                                static::error("No user definition found! [Level 3]");
                                $connection->close();
                            }

                        } else {

                            static::error("No user definition found! [Level 2]");
                            $connection->close();
                        }
                    }
                }


            };
        });
    }

    /**
     * @param  string  $id
     */
    static function online(string $id)
    {
        $list = static::onlineList();
        $list[$id] = $id;
        \Cache::forever(static::$ws_cache, $list);
    }

    /**
     * @param  string  $id
     */
    static function offline(string $id)
    {
        $list = static::onlineList();
        if (isset($list[$id])) {
            unset($list[$id]);
            \Cache::forever(static::$ws_cache, $list);
        }
    }

    /**
     * @return array
     */
    static function onlineList () {

        return \Cache::get(static::$ws_cache, []);
    }
}

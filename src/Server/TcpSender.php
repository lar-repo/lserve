<?php

namespace Lar\LServe\Server;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Lar\LServe\Passport;

class TcpSender
{
    const TO_ALL_CONNECTED              = "__TO_ALL_CONNECTED",                 //Send to all connected (Dangerous adventures are not considered).
        TO_CONNECTED_HAS_ID             = "__TO_CONNECTED_HAS_ID",              //Send to the connected who has an identifier.
        TO_CONNECTED_HAS_ID_LIST        = "__TO_CONNECTED_HAS_ID_LIST",         //Send to the connected who are in the list of identifiers.
        TO_ALL_ANONYMOUS                = "__TO_ALL_ANONYMOUS",                 //Send to all connected users who do not have an identifier.
        TO_DANGEROUS                    = "__TO_DANGEROUS",                     //Send to everyone who connected without verification.

        TO_GROUP_NAME                   = "__TO_GROUP_NAME",                    //Send to all users of the group.
        TO_GROUPS_LIST                  = "__TO_GROUPS_LIST",                   //Send to all users in the list of groups.
        TO_SYSTEM                       = "__TO_SYSTEM",                        //Send server command.
        TO_ME                           = "__TO_ME";                            //Send to only me. (It makes sense to use only in Ajax requests.)


    /**
     * Tcp resource
     *
     * @var bool|resource
     */
    protected static $instance = null;

    /**
     * Process passport
     *
     * @var array
     */
    protected static $process_passport = [];

    /**
     * Destination Recipients.
     *
     * @var string
     */
    protected $recipient;

    /**
     * Recipient data identifier
     *
     * @var string
     */
    protected $recipient_data;

    /**
     * Tcp init
     */
    public static function initializationInstance()
    {
        if (is_null(static::$instance)) {

            static::$instance = stream_socket_client(TcpServer::getLink());
        }
    }

    /**
     * Set a destination recipients.
     *
     * @param $recipient
     * @param string|null $data
     * @return TcpSender
     */
    public function setRecipient($recipient, $data = null): TcpSender
    {
        $this->recipient = $recipient;
        $this->recipient_data = is_array($data) ? $data : ($data ? [$data] : []);
        $this->recipient_data["ID"] = Passport::getMyId();

        return $this;
    }

    /**
     * Send data to recipient visitors
     *
     * @param mixed $data
     * @param array|null $conditions
     * @return bool|int
     * @throws \Exception
     */
    public function send($data = [], $conditions = [])
    {
        if (!static::ping())
            return false;

        if (is_null($this->recipient)) {

            throw new \Exception("Select a destination recipients.");
        }

        if ($data instanceof Arrayable) {

            $data = $data->toArray();
        }

        if (!is_array($data)) {

            $data = func_get_args();
        }

        if (static::$instance) {
            static::initializationInstance();
        }


        if ($conditions) {

            if (isset($conditions['CONN_ID']) && is_string($conditions['CONN_ID']) && !is_numeric($conditions['CONN_ID'])) {

                if ($conditions['CONN_ID'] === 'true') $conditions['CONN_ID'] = true;
                if ($conditions['CONN_ID'] === 'false') $conditions['CONN_ID'] = false;

            }
        }

        $str = json_encode(
            array_merge([$this->recipient => $this->recipient_data], $data, ["__CONDITIONS__" => $conditions]),
            JSON_UNESCAPED_UNICODE
        );

        $len = strlen($str);

        if ($len > 20000) {

            $key = md5($len.$str);
            \Cache::put($key, $str, now()->addMinutes(10));
            $str = $key;
        }

        return fwrite(static::$instance, $str, $len);
    }

    /**
     * @param $data
     * @return bool|false|int
     */
    public static function emit($data)
    {
        if (is_array($data)) {

            $data = json_encode($data);
        }

        if (!static::ping()) {

            return false;
        }

        return fwrite(static::$instance, $data, strlen($data));
    }

    public static function ping()
    {
        if (static::$instance === false) {

            return false;
        }

        try {
            static::initializationInstance();

            return true;

        } catch (\Exception $exception) {

            static::$instance = false;

            return false;
        }

    }

    public static function getJsConnection(){

        $link = WebSocketServer::getPublicLink([
            WebSocketServer::identifyValue() => Passport::getMyId()
        ]);

        $init_js = file_get_contents(__DIR__ . "/../../assets/initor.js");

        return str_replace(["__INITOR_WS_LINK__"], [$link], $init_js);

    }
}

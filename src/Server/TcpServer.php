<?php

namespace Lar\LServe\Server;


class TcpServer extends ProcessServer
{

    /**
     * WebSocketServer constructor.
     * 
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct(static::getLink());
    }

    /**
     * Protocol link
     *
     * @param array $params
     * @return string
     */
    public static function getLink(array $params = []){

        $ip = env("PROSERVER_TCP_IP", "127.0.0.1");
        $port = env("PROSERVER_TCP_PORT", 4460);

        return "tcp://{$ip}:{$port}".(count($params) ? "?".http_build_query($params) : "");
    }
}

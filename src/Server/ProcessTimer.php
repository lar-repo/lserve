<?php

namespace Lar\LServe\Server;

use Illuminate\Support\Arr;
use Workerman\Lib\Timer;

class ProcessTimer extends Timer
{
    const PING_INTERVAL_MINUTES = 30;

    const PING_TIMEOUT_MINUTES = 1;

    protected static $deleted_list = [];

    public static function custom()
    {
        if (env("PROSERVER_CRON", false) === true) {

            static::add(60, function () {

                WebSocketServer::comment(
                    exec("cd " . base_path() . " && php artisan schedule:run")
                );
            });
        }
    }

    /**
     * Start clear old connection timer
     */
    public static function cleaner()
    {
        static::add(60 * static::PING_INTERVAL_MINUTES, function () {

            WebSocketServer::$clients->map(function ($connection) {

                if (count($connection["QUEUE"])) {

                    return $connection;
                }

                if (isset($connection["IMMORTAL"])) {

                    WebSocketServer::comment("IMMORTAL ID [{$connection["ID"]}][Timeout " . static::PING_TIMEOUT_MINUTES . " min]");
                    return $connection;
                }

                if (($connection["TIMESTAMP"] + (60 * static::PING_TIMEOUT_MINUTES)) > time()) {

                    /** @var ConnectionWrapper $connection */
                    WebSocketServer::comment("Added to delete list [{$connection["ID"]}][Timeout " . static::PING_TIMEOUT_MINUTES . " min]");
                    static::$deleted_list[$connection["ID"]] = $connection["ID"];
                    WebSocketServer::offline($connection["ID"]);
                }

                return $connection;
            });
        });

        static::add(1, function () {

            if (count(static::$deleted_list)) {

                $id = Arr::last(static::$deleted_list);
                unset(static::$deleted_list[$id]);

                if (WebSocketServer::$clients->has($id)) {

                    /** @var ConnectionWrapper $connection */
                    $connection = WebSocketServer::$clients->get($id);

                    $connection->send(["\$js.\$ws.reConnect"], false);

                    WebSocketServer::$clients->except($id);

                    WebSocketServer::comment("Except ID [{$connection["ID"]}]");
                }
            }
        });
    }
}

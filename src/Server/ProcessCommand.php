<?php

namespace Lar\LServe\Server;

use Illuminate\Console\Command;

class ProcessCommand extends Command
{
    /**
     * ProcessCommand constructor.
     */
    public function __construct()
    {
        ProcessServer::commandInit($this);
        ProcessServer::$original_signature = explode(" ", $this->signature)[0];

        $this->signature = $this->signature . " {--debug : Debug mode} {--dev : Include special development executors}";
        parent::__construct();
    }
}

<?php

namespace Lar\LServe\Server;


use Illuminate\Support\Collection;
use Lar\LServe\Server\TcpCommands\TcpCommandInterface;

class TcpCommand implements TcpCommandInterface
{

    /**
     * TcpCommand constructor.
     *
     * @param array $props Filter settings to find the desired connection.
     * @param array $data Data to send to the connection.
     */
    public function __construct(array $props = [], array $data = [])
    {
        $this->queueDetector($props);
        $this->executorDetector($props);
    }

    protected function queueDetector($props)
    {
        $connection = WebSocketServer::$clients->get($props["ID"]);

        if (isset($props["QUEUE"])) {

            foreach ($props["QUEUE"] as $event => $queue) {

                if (substr($event, 0, 2) != "S:") {

                    $event = time().":del:".$event;
                }

                if (!isset($connection["QUEUE"])) {

                    $connection["QUEUE"] = new Collection();
                }

                $connection["QUEUE"]->put($event, $queue);
            }
        }
    }

    protected function executorDetector($props)
    {
        $connection = WebSocketServer::$clients->get($props["ID"]);

        if (isset($props["EXECUTORS"])) {

            if (is_array($props["EXECUTORS"])) {

                $execs = $connection["EXECUTORS"];

                foreach ($props["EXECUTORS"] as $key => $item) {

                    if (!class_exists($item)) {

                        WebSocketServer::error("Execute class [{$key}] => [{$item}] not exists!");

                    } else {

                        $execs[$key] = $item;
                        WebSocketServer::comment("New executor [{$key}] => [{$item}] from client [{$props["ID"]}]");
                    }
                }

                $connection["EXECUTORS"] = $execs;

            } else {

                WebSocketServer::error("Executor list is not array!");
            }
        }
    }

    public function send(array $data, array $conditions)
    {
        $clients = WebSocketServer::$clients;

        if (isset($conditions['CONN_ID'])) {

            $conn_id = $conditions['CONN_ID'];

            unset($conditions['CONN_ID']);

        } else {

            $conn_id = true;
        }

        if (isset($conditions['PERSON_ID']) && $conditions['PERSON_ID']) {

            if (is_array($conditions['PERSON_ID'])) {

                $clients = $clients->whereIn("ID", $conditions['PERSON_ID']);
            }

            else {

                $clients = $clients->where("ID", $conditions['PERSON_ID']);
            }

            unset($conditions['PERSON_ID']);
        }

        if (isset($conditions['GUARDED'])) {

            if ($conditions['GUARDED']) {

                $clients = $clients->where("GUARDED", '!=', 0);

            } else {

                $clients = $clients->where("GUARDED", 0);
            }

            unset($conditions['GUARDED']);
        }

        if (isset($conditions['GUARD'])) {

            $clients = $clients->filter(function ($item) use ($conditions) {

                return strpos($item["GUARDS"], $conditions["GUARD"]) !== false;
            });

            unset($conditions['GUARD']);
        }

        foreach ($conditions as $key => $condition) {

            if (is_array($condition)) {

                $clients = $clients->whereIn(strtoupper($key), $condition);

            } else {

                $clients = $clients->where(strtoupper($key), $condition);
            }
        }

        $clients->map(function ($connection) use ($data, $conn_id) {

            /** @var ConnectionWrapper $connection */
            $connection["TIMESTAMP"] = time();
            $connection->send($data, $conn_id);
        });
    }
}

<?php

namespace Lar\LServe\Server;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Workerman\Connection\TcpConnection;

class ConnectionWrapper implements \ArrayAccess, Arrayable
{

    /**
     * Some connection
     *
     * @var Collection
     */
    protected $connection;

    /**
     * Var connect collection
     *
     * @var array
     */
    protected $vars = [];

    /**
     * ConnectionWrapper constructor.
     *
     * @param TcpConnection $connection
     */
    public function __construct(TcpConnection $connection = null)
    {
        if (!$this->connection)
            $this->connection = new Collection();

        if ($connection)
            $this->setConnection($connection);
    }

    /**
     * Magic debug
     *
     * @return array
     */
    public function __debugInfo()
    {
        $vars = $this->vars;

        unset($vars["QUEUE"]);

        return array_merge($vars, [

            "CONN_COUNT" => $this->connection->count(),
            "CONN_KEYS" => $this->connection->keys(),
        ]);
    }

    /**
     * Connection getter
     *
     * @param $id
     * @return TcpConnection|bool
     */
    public function getConnection($id)
    {
        if ($id instanceof TcpConnection) $id = $id->id;

        if (!$this->connection->has($id)) {

            return false;
        }

        return $this->connection->get($id);
    }

    /**
     * Connection setter
     *
     * @param TcpConnection $connection
     * @return bool
     */
    public function setConnection(TcpConnection $connection)
    {
        $this->connection->put($connection->id, $connection);
        $this->vars["CURRENT_CONNECTION_ID"] = $connection->id;
        $this["TIMESTAMP"] = time();

        return true;
    }

    /**
     * Check queue's
     */
    public function queue()
    {
        if (isset($this["QUEUE"]) && $this["QUEUE"] instanceof Collection && $this["QUEUE"]->count()) {

            $this["QUEUE"] = $this["QUEUE"]->filter(function ($item, $key) {

                $del = false;

                $ex_key = explode(":", $key);
                $ex_key_count = count($ex_key);

                if ($ex_key_count == 3) {

                    if ($ex_key[1] == "del") $del = true;
                }

                $this->send([$ex_key[$ex_key_count-1] => $item], true);

                return !$del;
            });
        }
    }

    /**
     * Remove connection from collection of connections
     *
     * @param TcpConnection $connection
     * @return bool
     */
    public function removeConnection(TcpConnection $connection)
    {
        if ($this->connection->has($connection->id)) {

            $this->connection = $this->connection->except($connection->id);

            if (!$this->connection->count()) {

                WebSocketServer::offline($this["ID"]);
            }

            return true;
        }

        return false;
    }

    /**
     * Sends data on the connection.
     *
     * @param string|array $send_buffer
     * @param $conn_id
     * @param bool $raw
     * @return bool|null
     */
    public function send($send_buffer, $conn_id = false, $raw = false)
    {
        $send_buffer = !is_array($send_buffer) ? [$send_buffer] : $send_buffer;

        $send_buffer['server_time'] = time();

        $send_buffer = json_encode($send_buffer, JSON_UNESCAPED_UNICODE);

        if (!$this->connection->count())
            return false;

        if ($conn_id === true && $this->connection->has($this["CURRENT_CONNECTION_ID"])) {

            $this->connection->get($this["CURRENT_CONNECTION_ID"])->send($send_buffer, $raw);

        } else if ($conn_id === false) {

            $this->connection->map(function ($conn) use ($send_buffer, $raw) {

                /** @var TcpConnection $conn */
                $conn->send($send_buffer, $raw);
            });

        } else if (is_numeric($conn_id) && $this->connection->has($conn_id)) {

            $this->connection->get($conn_id)->send($send_buffer, $raw);

        } else if (is_array($conn_id)) {

            foreach ($conn_id as $id) {

                if ($this->connection->has($id)) {

                    $this->connection->get($id)->send($send_buffer, $raw);
                }
            }

        } else {

            return false;
        }

        return true;
    }


    /**
     * Whether a offset exists
     *
     * @param mixed $offset
     * @return boolean true on success or false on failure.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->vars[$offset]);
    }

    /**
     * Offset to retrieve
     *
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->vars[$offset];
    }

    /**
     * Array keys getter
     *
     * @return array
     */
    public function offsetKeys()
    {
        return array_keys($this->vars);
    }

    /**
     * Offset to set
     *
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset
     * @param mixed $value
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->vars[$offset] = $value;
    }

    /**
     * Offset to unset
     *
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->vars[$offset]);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $vars = $this->vars;

        unset($this["QUEUE"]["js_framework"]);

        return array_merge($vars, [

            "CONN_COUNT" => $this->connection->count(),
            "CONN_KEYS" => $this->connection->keys(),
        ]);
    }
}

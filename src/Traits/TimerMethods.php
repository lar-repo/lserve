<?php

namespace Lar\LServe\Traits;

use Carbon\Carbon;
use Lar\LServe\Server\TcpCommands\TimerRespond;
use Lar\LServe\Server\WebSocketServer;
use Workerman\Lib\Timer;

/**
 * Trait TimerGetters
 * @package Lar\LServe\Traits
 */
trait TimerMethods
{
    /**
     * @return bool
     */
    public function stop()
    {
        if ($this->timer_id) {

            return Timer::del($this->timer_id);
        }

        return false;
    }

    /**
     * @param  array  $conditions
     * @return TimerRespond
     */
    public function ws(array $conditions = [])
    {
        $respond = TimerRespond::create()->conditions($conditions);

        $this->sends[] = $respond;

        return $respond;
    }

    /**
     * @param $comment
     * @return $this
     */
    protected function info(string $comment)
    {
        WebSocketServer::info(' ' . $comment);

        return $this;
    }

    /**
     * @param $comment
     * @return $this
     */
    protected function error(string $comment)
    {
        WebSocketServer::info(' ' . $comment);

        return $this;
    }

    /**
     * @param $comment
     * @return $this
     */
    protected function comment(string $comment)
    {
        WebSocketServer::comment(' ' . $comment);

        return $this;
    }
}
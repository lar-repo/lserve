<?php

namespace Lar\LServe\Traits;

use Lar\LServe\LSValidator;

/**
 * Trait Validator
 *
 * @package Lar\LServe\Traits
 */
trait Validator
{
    /**
     * @param array $required
     * @param array $messages
     * @param array $titles
     * @return LSValidator
     * @throws \Exception
     */
    public function getVRequest(array $required = [], array $messages = [], array $titles = [])
    {
        if (!isset($this->valid)) {

            throw new \Exception("Before set a valid object");
        }

        $required_list = [];

        foreach ($required as $item) {

            $required_list[$item] = "required";
        }

        $valid = $this->valid;

        /** @var LSValidator $valid */
        return new $valid($required_list, $this, function (LSValidator $validator) use ($required, $messages, $titles) {

            if (count($required) == 1 && isset($required[0]) && $required[0] === "*") {

                $validator->setRequiredAll();
            }

            $validator->setMessages("required", $messages);
            $validator->titlesMerge($titles);
        });
    }
}
<?php

namespace Lar\LServe\Traits;

use Illuminate\Database\Eloquent\Model;

trait ValidatorHelpers
{
    /**
     * Set Modal
     *
     * @param Model $model
     * @return $this
     */
    public function setModel(Model $model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Set required from all fields
     *
     * @return $this
     */
    public function setRequiredAll()
    {
        foreach ($this->rules as $key => $rule) {

            $this->rules[$key] .= "|required";
        }

        return $this;
    }

    /**
     * Create data in to injected model
     *
     * @return bool
     */
    public function createInToModel()
    {
        if ($this->check() && $this->model instanceof Model) {

            return $this->model->create($this->toModel());
        }

        return false;
    }

    /**
     * Update data in to model
     *
     * @return bool
     */
    public function updateInToModel()
    {
        if ($this->check() && $this->model instanceof Model) {

            return $this->model->update($this->toModel());
        }

        return false;
    }

    /**
     * Write in to model and detect update or create
     *
     * @return bool
     */
    public function inToModel()
    {
        if ($this->check() && $this->model instanceof Model) {

            if ($this->model->exists) {

                return $this->updateInToModel();

            } else {

                return $this->createInToModel();
            }
        }

        return false;
    }

    /**
     * Set messages on type
     *
     * @param string $type
     * @param array $messages
     * @return $this
     */
    public function setMessages(string $type, array $messages)
    {
        foreach ($messages as $key => $message) {

            $this->messages[$key . "." . $type] = $message;
        }

        return $this;
    }

    /**
     * Merge title list
     *
     * @param array $titles
     * @return $this
     */
    public function titlesMerge(array $titles)
    {
        $this->titles = array_merge($this->titles, $titles);

        return $this;
    }
}
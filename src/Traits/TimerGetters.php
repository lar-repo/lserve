<?php

namespace Lar\LServe\Traits;

use Carbon\Carbon;

/**
 * Trait TimerGetters
 * @package Lar\LServe\Traits
 */
trait TimerGetters
{
    /**
     * @return Carbon
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * @return int
     */
    public function getSeconds()
    {
        return $this->seconds;
    }

    /**
     * @return bool
     */
    public function getPersistent()
    {
        return $this->persistent;
    }

    /**
     * @return Carbon
     */
    public function getDieAt()
    {
        return $this->die_at;
    }
}
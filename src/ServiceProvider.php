<?php

namespace Lar\LServe;

use Illuminate\Support\ServiceProvider as ServiceProviderIlluminate;
use Lar\LServe\Commands\ExecutorMake;
use Lar\LServe\Commands\SayServer;
use Lar\LServe\Middleware\Registration;
use Lar\LServe\Middleware\WsUser;
use Lar\LServe\Server\TcpSender;
use Lar\Tagable\Tag;

/**
 * Class ServiceProvider
 *
 * @package Lar\LServe
 */
class ServiceProvider extends ServiceProviderIlluminate
{
    /**
     * @var array
     */
    protected $commands = [
        \Lar\LServe\Commands\LServe::class,
        SayServer::class,
        ExecutorMake::class
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        "ws_user" => WsUser::class,
        "lserve" => Registration::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'lar' => [

        ],
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws \Exception
     */
    public function boot()
    {
        LServe::executors();

        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__.'/../config' => config_path()], 'lar-lserve');
        }

        Tag::injectFile(__DIR__ . "/components.php");
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/lserve.php', 'lserve'
        );

        $this->registerRouteMiddleware();
        $this->commands($this->commands);
        $this->dispatchedWsTimers();
    }

    protected function dispatchedWsTimers()
    {
        register_shutdown_function(function () {

            if (LServe::$timers) {

                $timers = [];

                foreach (LServe::$timers as $key => $timer) {

                    $timer = serialize($timer);

                    $timers[md5($timer)] = $timer;
                }
                
                \Cache::forever('ws:timers', $timers);

                TcpSender::emit('make_timers');
            }
        });
    }

    /**
     * Register the route middleware.
     *
     * @return void
     */
    protected function registerRouteMiddleware()
    {
        // register route middleware.
        foreach ($this->routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }

        // register middleware group.
        foreach ($this->middlewareGroups as $key => $middleware) {
            app('router')->middlewareGroup($key, $middleware);
        }
    }
}


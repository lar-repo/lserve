<?php

namespace Lar\LServe;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Lar\LServe\Traits\ValidatorHelpers;

class LSValidator extends Collection
{
    use ValidatorHelpers;

    /**
     * Validate data rules
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Validate data
     *
     * @var array
     */
    protected $validate_data = [];

    /**
     * Error messages
     *
     * @var array
     */
    protected $messages = [];

    /**
     * Message titles
     *
     * @var array
     */
    protected $titles = [];

    /**
     * Custom Attributes
     *
     * @var array
     */
    protected $customAttributes = [];

    /**
     * Error messages
     * 
     * @var array 
     */
    protected $error_messages = [];

    /**
     * To model only data
     *
     * @var array
     */
    protected $only = [];

    /**
     * Validator
     *
     * @var \Illuminate\Validation\Validator
     */
    protected $validator;

    /**
     * Model link
     *
     * @var null
     */
    public $model = null;

    /**
     * Model data
     *
     * @var array
     */
    protected $model_data = [];

    /**
     * Toastr message timeout
     *
     * @var int
     */
    protected $toastr_timeout = 10000;

    /**
     * LSValidator constructor.
     *
     * @param null $model
     * @param null|array|Arrayable $validate_data
     * @param null $rules
     * @throws \Exception
     */
    public function __construct($model = null, $validate_data = null, $rules = null)
    {
        foreach (func_get_args() as $arg) {

            if ($arg instanceof \Closure) {

                $arg($this);

            } else if ($arg instanceof Model) {

                $this->model = $arg;

            } else if ($arg instanceof Request) {

                $this->validate_data = $arg->all();

            } else if (is_array($arg)) {

                foreach ($arg as $key => $item) {

                    if (isset($this->rules[$key])) {

                        $this->rules[$key] .= "|" . $item;

                    } else {

                        $this->rules[$key] = $item;
                    }
                }

                $this->only = [];
            }
        }

        parent::__construct();

        if ($this->model && is_string($this->model)) {

            $this->model = new $this->model;
        }

        /**
         * If don't have valid data, use a default process request
         */
        if (!count($this->validate_data)) {

            $this->validate_data = app('request')->all();
        }

        /**
         * Exception if don't have rules
         */
        if (!count($this->rules)) {

            throw new \Exception("First you need to set validation rules.");
        }
        
        $this->makeValidator();

        $this->toFillCollection();

        $this->sendMessages();
    }

    /**
     * Get data from model
     *
     * @param Model|null $model
     * @return array
     * @throws \ReflectionException
     */
    public function toModel($model = null)
    {
        if ($model) {

            if (is_string($model)) {

                $model = new $model;
            }

            $this->model = $model;
        }


        $this->only = array_merge(
            $this->only,
            $this->getModelFillables()
        );

        foreach ($this->only as $item) {

            if (isset($this->items[$item]) || method_exists($this, "get" . ucfirst(Str::camel($item)) . "Attribute")) {

                $this->model_data[$item] = $this->{$item};
            }
        }

        return $this->model_data;
    }

    /**
     * To real data
     *
     * @return array
     * @throws \ReflectionException
     */
    public function toReal()
    {
        if (!count($this->model_data)) {

            $this->toModel();
        }

        if ($this->model && count($this->model_data)) {

            $datas = [];

            foreach ($this->model_data as $key => $data) {

                if (!is_null($this->model->{$key})) {

                    if ($this->model->{$key} != $data) {

                        $datas[$key] = $data;
                    }

                } else {

                    $datas[$key] = $data;
                }
            }

            return $datas;
        }

        return [];
    }

    /**
     * mmm ... well, yes, I break the rules a little =) well, I can probably do it
     *
     * @throws \ReflectionException
     */
    public function getModelFillables()
    {
        if (!$this->model) {

            return [];
        }

        $ref = new \ReflectionClass($this->model);

        if ($ref->hasProperty("fillable")) {

            $prop = $ref->getProperty("fillable");

            $prop->setAccessible(true);

            return $prop->getValue($this->model);
        }

        return [];
    }

    /**
     * Check on error messages
     *
     * @return bool
     */
    public function check()
    {
        return !count($this->error_messages);
    }

    /**
     * Validator maker
     */
    final private function makeValidator()
    {
        $this->validator = Validator::make($this->validate_data, $this->rules, $this->messages, $this->customAttributes);

        $this->error_messages = $this->validator->messages()->messages();
    }

    /**
     * To fill collection
     */
    protected function toFillCollection()
    {
        foreach ($this->rules as $key => $val) {

            if (!isset($this->error_messages[$key]) && isset($this->validate_data[$key])) {

                $this->{$key} = $this->validate_data[$key];
            }
        }
    }

    /**
     * Messages sender
     */
    protected function sendMessages()
    {
        foreach ($this->error_messages as $title => $message) {

            if (isset($this->titles[$title])) {

                $title = $this->titles[$title];

            } else {

                $title = trim(ucwords(str_replace(["_", "-", "[", "]"], " ", $title)));
            }

            $this->messageSender($title, $message);
        }
    }

    /**
     * Message sender method
     *
     * @param $title
     * @param $message
     * @throws \Exception
     */
    protected function messageSender($title, $message)
    {
        inAllDisplay([
            "toastr" => [
                "error" => [
                    implode("<br>", $message),
                    $title,
                    ["timeOut" => $this->toastr_timeout]
                ]
            ]
        ]);
    }

    /**
     * Attribute mutators
     *
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public function __get($key)
    {
        $mutatorName = "get" . ucfirst(Str::camel($key)) . "Attribute";

        if (method_exists($this, $mutatorName)) {

            return $this->{$mutatorName}(isset($this->items[$key]) ? $this->items[$key] : null);
        }

        if (isset($this->items[$key])) {

            return $this->items[$key];
        }

        return parent::__get($key);
    }

    /**
     * Attribute mutators
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $mutatorName = "set" . ucfirst(Str::camel($name)) . "Attribute";

        if (method_exists($this, $mutatorName)) {

            $this->put($name, $this->{$mutatorName}($value));

        } else {

            $this->put($name, $value);
        }
    }

    /**
     * @return array
     */
    public function __debugInfo()
    {
        return $this->items;
    }

    /**
     * @param array $data
     * @return LSValidator
     * @throws \Exception
     */
    public static function create(...$data)
    {
        return new static(...$data);
    }
}

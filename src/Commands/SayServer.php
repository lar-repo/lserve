<?php

namespace Lar\LServe\Commands;

use Illuminate\Console\Command;
use Lar\LServe\Server\TcpCommands\SendToAll;
use Lar\LServe\Server\TcpSender;

class SayServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lar:say_serve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Virtual Reactive Test Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {

        /*(new TcpSender())->setRecipient(SendToAll::class)->send([
            "$" => [
                "html::.notification-item__number" => "eval::Number($('.notification-item__number').html()) + 1"
            ]
        ]);*/

        /*$channel = $this->choice(
            'What channel to send a message?',
            [TcpSender::TO_ALL_CONNECTED, TcpSender::TO_ALL_ANONYMOUS, TcpSender::TO_GROUP_NAME, TcpSender::TO_CONNECTED_HAS_ID]
        );

        $for = false;

        if ($channel == TcpSender::TO_GROUP_NAME) {

            $for = $this->ask('Enter the group name');
        }

        if ($channel == TcpSender::TO_CONNECTED_HAS_ID) {

            $for = $this->ask('Specify connection ID');
        }

        TcpSender::to($channel, $for)->send($this->ask('What do you want to say?'));*/
    }
}

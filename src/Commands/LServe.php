<?php

namespace Lar\LServe\Commands;

use Lar\LServe\Server\ProcessCommand;
use Lar\LServe\Server\WebSocketServer;

class LServe extends ProcessCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lar:serve ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Virtual Reactive PHP Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->option("dev")) {

            $this->error("Attention! Developer mode is enabled, which means that your code is not secure.");
        }

        //event to start
        (new WebSocketServer())->runWithTcp();
        //event to bye
    }
}

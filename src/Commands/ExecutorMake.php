<?php

namespace Lar\LServe\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Lar\LServe\Executor;
use Lar\LServe\Server\WebSocketServer;

class ExecutorMake extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lar:ws_executor {name} {--off : don\'t add to config}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new LServe executor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $obj_name = $this->argument("name");

        $path = app_path("Http/WsExecutors/");

        $namespace = "App\\Http\\WsExecutors";

        $_t_path = [];

        if (($_t_name = dirname($obj_name)) !== ".") {

            $obj_name = trim(str_replace($_t_name, "", $obj_name), "/");

            $_t_path = array_map("ucfirst", explode("/", $_t_name));

            $path .= implode("/", $_t_path) . "/";

            $namespace .= "\\" . implode("\\", $_t_path);
        }

        if (!is_dir($path)) {

            mkdir($path, 0777, true);
        }


        $obj_name = ucfirst(Str::camel($obj_name));

        $executor_name = $obj_name;

        $obj = class_entity($obj_name);

        $obj->wrap('php')
            ->namespace($namespace)
            ->extend(Executor::class);

        $obj->prop("name", $executor_name);

        $obj->method("__invoke")
            ->line()
            ->line(WebSocketServer::class . "::info('My Executor [{$executor_name}]');")
            ->line()
            ->line("return ;");

        file_put_contents($path . $obj_name . ".php", $obj->render());

        $this->info("WS Executor created [" . str_replace(base_path(), "", $path) . "{$obj_name}.php]");

        if (!$this->option('off')) {

            add_to_config($file = config_path("ws_executors.php"), $executor_name, "{$namespace}\\{$obj_name}");

            $this->info("WS Executor added to the global config [{$file}].");
        }
    }
}

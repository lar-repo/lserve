<?php

if (!function_exists('ws')) {

    /**
     * @param  string|null  $with
     * @param  null  $value
     * @return \Lar\LServe\WsRespond
     */
    function ws (string $with = null, $value = null) {

        return \Lar\LServe\WsRespond::create()->with($with, $value);
    }
}

if (!function_exists("add_executor")) {

    /**
     * Add executor to the client session
     *
     * @param $classes
     * @return bool
     * @throws Exception
     */
    function add_executor($classes) {

        if (!\Lar\LServe\Server\TcpSender::ping()) {

            return false;
        }

        if (!is_array($classes)) {

            $classes = func_get_args();
        }

        foreach ($classes as $key => $class) {

            if (!class_exists($class)) {

                throw new Exception("Class [$class] not exists");
            }

            $obj = new $class;

            if (!$obj instanceof \Lar\LServe\Executor) {

                throw new Exception("Class [$class] not executor");
            }

            unset($classes[$key]);
            $classes[$obj->name] = $class;
        }

        (new \Lar\LServe\Server\TcpSender())
            ->setRecipient(\Lar\LServe\Server\TcpCommand::class, ["EXECUTORS" => $classes])
            ->send([]);
    }
}

if (!function_exists("console_log")) {

    /**
     * Send info to console.log
     *
     * @param $data
     * @param array $conditions
     * @return bool
     * @throws Exception
     */
    function console_log ($data, $conditions = []) {

        return send_data(["ljs._log" => $data], $conditions);
    }
}


if (!function_exists('send_data')) {

    /**
     * Send data in to browser
     *
     * @param $data
     * @param array $conditions
     * @param array $queue
     * @return bool
     * @throws Exception
     */
    function send_data($data, $conditions = [], $queue = []) {

        if (!\Lar\LServe\Server\TcpSender::ping()) {

            return false;
        }

        if (count($queue)) {

            $queue = ["QUEUE" => $queue];
        }

        (new \Lar\LServe\Server\TcpSender())->setRecipient(\Lar\LServe\Server\TcpCommand::class, $queue)->send($data, $conditions);

        return true;
    }
}

if (!function_exists('send_to_console')) {

    /**
     * Send data in to console
     *
     * @param $data
     * @return bool
     * @throws Exception
     */
    function send_to_console($data) {

        if (!\Lar\LServe\Server\TcpSender::ping()) {

            return false;
        }

        (new \Lar\LServe\Server\TcpSender())->setRecipient(\Lar\LServe\Server\TcpCommands\SendToConsole::class)->send(["home" => $data]);
    }
}

if (!function_exists("inDisplay")) {

    /**
     * Send data in to current window
     *
     * @param $data
     * @param array $conditions
     * @return bool
     * @throws Exception
     */
    function inDisplay($data, $conditions = []) {

        if (request()->ajax() || request()->pjax()) {

            send_data($data, array_merge(["CONN_ID" => true], $conditions));

        } else {

            send_data(["QUEUE"], [], $data);
        }
    }
}

if (!function_exists("inAllDisplay")) {

    /**
     * Send data in to all window
     *
     * @param $data
     * @param array $conditions
     * @return bool
     * @throws Exception
     */
    function inAllDisplay($data, $conditions = []) {

        if (request()->ajax() || request()->pjax()) {

            send_data($data, array_merge(["CONN_ID" => false], $conditions));

        } else {

            send_data([], [], $data);
        }
    }
}

if (! function_exists("execute_js")) {

    /**
     * Execute JavaScript
     *
     * @param $js
     * @param string $mode
     * @throws Exception
     */
    function execute_js($js, $mode = "$::eval") {

        if (!request()->ajax()) {

            inDisplay([$mode => $js]);

        } else {

            \App\Shape\Layout::$js_inits[] = $js;
        }
    }
}


if (!function_exists("wsGoTo")) {

    /**
     * @param $route
     * @param array $params
     * @throws Exception
     */
    function wsGoTo($route, $params = []) {

        $url = null;

        if (strpos($route, "/") === false) {

            $url = "/" . trim(str_replace(env("APP_URL"), "", route($route, $params)), "/");
        }

        $cond = [];

        if (!\Auth::guest()) {

            $cond["WEB_ID"] = \Auth::id();
        }

        inDisplay(["\$js.\$nav.goTo" => $url], $cond);
    }
}

if (!function_exists("add_to_config")) {

    /**
     * Add config line in to array
     *
     * @param $key
     * @param $value
     */
    function add_to_config($file, $key, $value) {

        $dir = dirname($file);

        if (!is_dir($dir)) {

            mkdir($dir, 0777, true);
        }

        $data = [];

        if (is_file($file)) {

            $data = include $file;
        }

        if (is_string($key) || is_numeric($key)) {

            $data[$key] = $value;
        }

        else {

            $data[] = $value;
        }

        file_put_contents($file, config_file_wrapper($data));
    }
}

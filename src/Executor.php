<?php

namespace Lar\LServe;

use Lar\Layout\Respond;
use Lar\LServe\Server\ConnectionWrapper;
use Lar\LServe\Server\WebSocketServer;

class Executor
{
    /**
     * Command sender
     *
     * @var ConnectionWrapper
     */
    public $sender;

    /**
     * @var Respond
     */
    public $respond;

    /**
     * Sender ID
     *
     * @var string
     */
    protected $sender_id;

    /**
     * Received data from the sender.
     *
     * @var mixed
     */
    protected $data;

    /**
     * Label cancel executor
     *
     * @var bool
     */
    protected $validate = true;

    /**
     * Executor constructor.
     */
    public function __construct()
    {
        $this->respond = new Respond();
    }

    /**
     * Clients collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function clients()
    {
        return WebSocketServer::$clients;
    }

    /**
     * Console info message
     * 
     * @param $message
     * @return $this
     */
    public function info($message)
    {
        WebSocketServer::info($message);
        
        return $this;
    }

    /**
     * Console comment message
     * 
     * @param $message
     * @return $this
     */
    public function comment($message)
    {
        WebSocketServer::comment($message);
        
        return $this;
    }

    /**
     * Console question message
     *
     * @param $message
     * @return $this
     */
    public function question($message)
    {
        WebSocketServer::question($message);

        return $this;
    }

    /**
     * Console error message
     *
     * @param $message
     * @return $this
     */
    public function error($message)
    {
        WebSocketServer::error($message);

        return $this;
    }

    /**
     * Console line message
     *
     * @param $message
     * @return $this
     */
    public function line($message)
    {
        WebSocketServer::line($message);

        return $this;
    }

    /**
     * Set sender
     *
     * @param ConnectionWrapper $wrapper
     * @return Executor
     * @deprecated Only from server (non't use)
     */
    public function setSender(ConnectionWrapper $wrapper)
    {
        $this->sender = $wrapper;

        return $this;
    }

    /**
     * Set data
     *
     * @param $data
     * @return $this
     * @deprecated Only from server (non't use)
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set sender ID
     *
     * @param $sender_id
     * @return $this
     * @deprecated Only from server (non't use)
     */
    public function setSenderId($sender_id)
    {
        $this->sender_id = $sender_id;

        return $this;
    }

    /**
     * Get validate result
     *
     * @return bool
     */
    public function isValid() : bool
    {
        return $this->validate;
    }

    /**
     * Magic getter
     *
     * @param $name
     * @return string
     */
    public function __get($name)
    {
        if ($name == "name" && !isset($this->name)) {

            $class_name = last_namespace_element(static::class);
            return trim(strtolower(preg_replace("/([A-Z]{1})/", ".$1", $class_name)), ".");
        }
    }
}

<?php

namespace Lar\LServe;

/**
 * Class LServe
 *
 * @package Lar\LServe
 */
final class LServe
{
    /**
     * @var ExecutorsCollection
     */
    static $executors;

    /**
     * @var array
     */
    static $timers = [];

    /**
     * Init executors
     */
    static function executors()
    {
        if (!static::$executors) {

            static::$executors = new ExecutorsCollection();
        }

        return static::$executors;
    }

    /**
     * Add executor in list
     *
     * @param string $class
     */
    static function addExecutor(string $class)
    {
        $class_name = last_namespace_element($class);

        $executor_name = trim(strtolower(preg_replace("/([A-Z]{1})/", ".$1", $class_name)), ".");

        static::executors()->put($executor_name, $class);
    }
}
